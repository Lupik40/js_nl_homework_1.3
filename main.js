/*
    TASK 1

    Вывести в консоль с помощью цикла  WHILE, все нечетные числа от 30 до 40

*/

let num = 30;

while(num < 40) {
    num++;
    if(num % 2 == 0) continue;
    console.log(num);
}

/*
    TASK 2

    Вывести в консоль с помощью цикла FOR, все четные числа от 70 до 40.
    Первым четным числом должно быть 70. 
    

*/

for(let i = 70; i >= 40; i--) { 
    if(i % 2 != 0) continue;
    console.log(i);
}

/*
    TASK 3

    Переведите цикл FOR из задания 2, в цикл WHILE.  
    
*/

let i = 72;

while(i > 40) {
    i--;
    if(i % 2 != 0) continue;
    console.log(i);
}

/*
    TASK 4

    С помощью любого известного Вам цикла из JavaScript, 
    найдите сумму чисел от 1 до 100, и выведите результат в консоль.   
    
*/

let j = 0;

for(let i = 0; i <= 100; i++) {
    j = j + i;
}
console.log(j);

/*
    TASK 5

    Вывести 10 строчек со смайликом : ':)'. 
    На первой строчке один смайлик, на второй два,и так далее. 
    На последней должно быть 10 смайликов.   
    
*/

let smile = ':)';

for(let i = 0; i < 10; i++) {
    console.log(smile);
    smile = smile + ':)';
}

/*
    TASK 6

    Напишите программу, которая выводит на экран числа от 1 до 50. 
    При этом вместо чисел, кратных трем, программа должна выводить слово «Java»,
    а вместо чисел, кратных пяти — слово «Script». 
    Если число кратно и 3, и 5, то программа должна выводить слово «JavaScript»   
    
*/

let jsNumber = 0;

while(jsNumber < 50) {
    jsNumber++;
    if (jsNumber % 3 == 0 && jsNumber % 5 == 0) {
        console.log('JavaScript');
    } else if (jsNumber % 3 == 0) {
        console.log('Java');
    } else if (jsNumber % 5 == 0) {
        console.log('Script');
    }  else {
        console.log(jsNumber);
    }
}

/* 
    TASK 7
    У нас есть число 1000
    Делите его на 2 столько раз, пока результат деления не станет меньше 50.
    Посчитайте количество итераций необходимых для выхода из цикла 
    и выведите результат в консоль

*/

let number = 1000;
let amount = 0;

while(number > 50) {
    number = number / 2;
    amount++;
}

console.log(amount);

/*
    TASK 8
    Напишите программу, которая используя цикл while 
    выведет все числа от 45 до 170, кратные 10.

*/

let lastNum = 45;

while(lastNum < 170) {
    lastNum++;
    if(lastNum % 10 != 0) continue;
    console.log(lastNum);
}
